<?php
/*
+------+---------------+--------+-----------+------------+--------------------+-----------+
| nip  | nama          | gender | tmp_lahir | tgl_lahir  | email              | divisi_id |
+------+---------------+--------+-----------+------------+--------------------+-----------+
*/
    require_once "DAO_historypendidikan.php";
    class history_pendidikan extends DAO_historypendidikan
    {
        public function __construct()
        {
            parent::__construct("history_pendidikan");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id, tahun_lulus, sekolah, jurusan, noijazah, nip, jenjang_id) " .
            " VALUES (?,?,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET id=?, tahun_lulus=?, sekolah=?, jurusan=?, noijazah=?, nip=?, jenjang_id=?" .
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik

    }
?>
