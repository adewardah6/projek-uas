<?php
    require_once 'db/classpegawai.php';
/*
+------+---------------+--------+-----------+------------+--------------------+-----------+
| nip  | nama          | gender | tmp_lahir | tgl_lahir  | email              | divisi_id |
+------+---------------+--------+-----------+------------+--------------------+-----------+
*/
    $obj = new pegawai();

    $_nip = $_POST['nip'];
    $_nama = $_POST['nama'];
    $_gender = $_POST['gender'];
    $_tmp_lahir = $_POST['tmp_lahir'];
    $_tgl_lahir = $_POST['tgl_lahir'];
    $_email = $_POST['email'];
    $_divisi_id = $_POST['divisi_id'];
    $_proses = $_POST['proses'];

    $ar_data[] = $_nip;
    $ar_data[] = $_nama;
    $ar_data[] = $_gender;
    $ar_data[] = $_tmp_lahir;
    $ar_data[] = $_tgl_lahir;
    $ar_data[] = $_email;
    $ar_data[] = $_divisi_id;

    //buat operasi jika memilih button simpan, update atau hapus
    $row = 0;
    if($_proses == "Simpan"){
        $row = $obj->simpan($ar_data);
    }elseif($_proses == "Update"){
        $_idedit = $_POST['idedit'];
        $ar_data[] = $_idedit;
        $row = $obj->ubah($ar_data);
    }elseif($_proses == "Hapus"){
        unset($ar_data);
        $_idedit = $_POST['idedit'];
        $row = $obj->hapus($_idedit);
    }
    //handeler jika gagal atau sukses
    if($row==0){
        echo "Gagal Proses";
    }else{
        //echo "Proses Sukses";
        //langsung direct ke daftar_kegiatan.php
        header('Location:indexpegawai.php');
    }
