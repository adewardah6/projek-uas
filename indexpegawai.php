<?php

/*+------+---------------+--------+-----------+------------+--------------------+-----------+
| nip  | nama          | gender | tmp_lahir | tgl_lahir  | email              | divisi_id |
+------+---------------+--------+-----------+------------+--------------------+-----------+
*/
    include_once 'top.php';
    require_once 'db/classpegawai.php';
    $obj = new pegawai();
    $rows = $obj->getAll();
    ?>

    <!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
    <h2 align="center">Daftar Pegawai</h2>
    <div class="line-dec"></div>
    <script type="text/javascript">
        $(document).ready(function(){
          $('#mahasiswa').DataTable();
        });
    </script>

    <table id="mahasiswa" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
        <thead>
        <tr class="danger">
            <th>NO</th><th>NIP</th><th>NAMA</th><th>GENDER</th><th>Tempat Lahir</th><th>Tanggal Lahir</th><th>email</th><th>divisi</th><<th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $nomor = 1;
        foreach($rows as $row){
            echo '<tr><td>'.$nomor.'</td>';
            echo '<td class="success">'.$row['nip'].'</td>';
            echo '<td>'.$row['nama'].'</td>';
            echo '<td class="success">'.$row['gender'].'</td>';
            echo '<td>'.$row['tmp_lahir'].'</td>';
            echo '<td class="success">'.$row['tgl_lahir'].'</td>';
            echo '<td>'.$row['email'].'</td>';
            echo '<td class="success">'.$row['divisi_id'].'</td>';
            echo '<td><a href="viewpegawai.php?id='.$row['nip']. '">View</a> |';
            echo '<a href="form_pegawai.php?id='.$row['nip']. '">Update</a></td>';
            echo '</tr>';
           $nomor++;
        }
        ?>
        </tbody>
    </table>
    <div class="panel-header" class="col-md-3">
        <a class="btn icon-btn btn-primary" href="form_pegawai.php">
        <span class="glyphicon btn-glyphicon glyphicon-plus img-circle text-warning"></span>Tambah Mahasiswa</a>

</div>
