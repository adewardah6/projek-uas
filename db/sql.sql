-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 18, 2017 at 02:53 AM
-- Server version: 10.1.9-MariaDB-log
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbhrd`
--

-- --------------------------------------------------------

--
-- Table structure for table `absensi`
--

CREATE TABLE `absensi` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `jam_masuk` time DEFAULT NULL,
  `jam_keluar` time DEFAULT NULL,
  `keterangan` varchar(45) DEFAULT NULL,
  `nip` varchar(10) NOT NULL,
  `shift_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `absensi`
--

INSERT INTO `absensi` (`id`, `tanggal`, `jam_masuk`, `jam_keluar`, `keterangan`, `nip`, `shift_id`) VALUES
(1, '2017-12-01', '07:20:00', '16:30:00', '-', '0120', 1),
(2, '2017-12-02', '07:20:00', '16:36:00', '-', '0120', 1),
(3, '2017-12-03', '07:35:00', '16:30:00', '-', '0120', 1),
(4, '2017-12-01', '07:30:00', '16:30:00', '-', '0121', 1),
(5, '2017-12-02', '07:30:00', '16:30:00', '-', '0121', 1),
(6, '2017-12-03', '07:30:00', '16:30:00', '-', '0121', 1),
(7, '2017-12-04', '07:30:00', '16:30:00', '-', '0121', 1),
(8, '2017-12-01', '07:30:00', '17:00:00', '-', '0161', 2),
(9, '2017-12-02', '07:30:00', '17:00:00', '-', '0161', 2),
(10, '2017-12-03', '07:30:00', '17:00:00', '-', '0161', 2);

-- --------------------------------------------------------

--
-- Table structure for table `divisi`
--

CREATE TABLE `divisi` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `divisi`
--

INSERT INTO `divisi` (`id`, `nama`, `alamat`) VALUES
(1, 'Biro Kepegawaian', 'Gedung B Lantai 3'),
(2, 'Biro Keuangan', 'Gedung D Lantai 2'),
(3, 'Biro Sarana Prasarana', 'Gedung B Lantai 6'),
(4, 'Biro Perencanaan', 'Gedung D Lantai 4'),
(5, 'Biro Umum', 'Gedung B Lantai 2');

-- --------------------------------------------------------

--
-- Table structure for table `history_pendidikan`
--

CREATE TABLE `history_pendidikan` (
  `id` int(11) NOT NULL,
  `tahun_lulus` int(11) DEFAULT NULL,
  `sekolah` varchar(45) DEFAULT NULL,
  `jurusan` varchar(45) DEFAULT NULL,
  `noijazah` varchar(20) DEFAULT NULL,
  `nip` varchar(10) NOT NULL,
  `jenjang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `history_pendidikan`
--

INSERT INTO `history_pendidikan` (`id`, `tahun_lulus`, `sekolah`, `jurusan`, `noijazah`, `nip`, `jenjang_id`) VALUES
(1, 2012, 'STT-NF', 'Teknik Informatika', 'NF013010', '0161', 2),
(2, 2015, 'Universitas Indonesia', 'Ilmu Komputer', 'UI20101', '0161', 3),
(3, 2010, 'Poltek Negeri Jakarta', 'Desain Komunikasi Visual', 'PNJ0101', '0201', 1),
(4, 2012, 'STT-NF', 'Sistem Informasi', 'NF40012', '0201', 2);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_kegiatan`
--

CREATE TABLE `jenis_kegiatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jenis_kegiatan`
--

INSERT INTO `jenis_kegiatan` (`id`, `nama`) VALUES
(1, 'Pelatihan'),
(2, 'Wisata'),
(3, 'Seminar'),
(4, 'Rapat Kerja');

-- --------------------------------------------------------

--
-- Table structure for table `jenjang_pendidikan`
--

CREATE TABLE `jenjang_pendidikan` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jenjang_pendidikan`
--

INSERT INTO `jenjang_pendidikan` (`id`, `nama`) VALUES
(1, 'Diploma 3'),
(2, 'Strata 1'),
(3, 'Strata 2'),
(4, 'Strata 3');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_cuti`
--

CREATE TABLE `kategori_cuti` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `maksimal` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori_cuti`
--

INSERT INTO `kategori_cuti` (`id`, `nama`, `maksimal`) VALUES
(1, 'Regular', 12),
(2, 'Melahirkan', 60),
(3, 'Haji', 30);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_lembur`
--

CREATE TABLE `kategori_lembur` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kategori_lembur`
--

INSERT INTO `kategori_lembur` (`id`, `nama`) VALUES
(1, 'Rutin'),
(2, 'Project'),
(3, 'Tugas Luar');

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `peserta` int(11) DEFAULT NULL,
  `tempat` varchar(45) DEFAULT NULL,
  `budget` double DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `nip_pic` varchar(10) NOT NULL,
  `jenis_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`id`, `kode`, `judul`, `peserta`, `tempat`, `budget`, `tgl_mulai`, `tgl_selesai`, `nip_pic`, `jenis_id`) VALUES
(1, 'K101', 'Seminar Parenting Pegawai', 100, 'Aula Gedung B', 25000000, '2017-12-01', '2017-12-01', '0151', 3),
(2, 'K102', 'Seminar Menjelang Pensiun', 40, 'Aula Gedung B', 35000000, '2017-12-03', '2017-12-03', '0201', 3),
(3, 'K201', 'Outbond 2017', 150, 'Ciapus Bogor', 75000000, '2017-12-26', '2017-12-27', '0121', 3);

-- --------------------------------------------------------

--
-- Table structure for table `lembur`
--

CREATE TABLE `lembur` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `mulai` time DEFAULT NULL,
  `akhir` time DEFAULT NULL,
  `keterangan` varchar(45) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1 diajukan 2 disetujui 3 ditolak',
  `nip` varchar(10) NOT NULL,
  `kategori_lembur_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lembur`
--

INSERT INTO `lembur` (`id`, `tanggal`, `mulai`, `akhir`, `keterangan`, `status`, `nip`, `kategori_lembur_id`) VALUES
(1, '2017-11-10', '17:00:00', '21:00:00', 'membuat laporan bulanan', 1, '0161', 1),
(2, '2017-12-10', '17:00:00', '23:00:00', 'dateline proyek web', 1, '0151', 2),
(3, '2017-11-30', '17:00:00', '20:00:00', 'membuat laporan gaji', 1, '0161', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `nip` varchar(10) NOT NULL,
  `nama` varchar(45) NOT NULL,
  `gender` char(1) NOT NULL,
  `tmp_lahir` varchar(20) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `divisi_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`nip`, `nama`, `gender`, `tmp_lahir`, `tgl_lahir`, `email`, `divisi_id`) VALUES
('0120', 'Dudi Herlina', 'L', 'Depok', '1990-04-01', 'dudi@gmail.com', 1),
('0121', 'Nia Dania', 'P', 'Bogor', '1993-09-12', 'niadania@gmail.com', 1),
('0122', 'Nana Krisna', 'L', 'Bekasi', '1999-01-11', 'nanakrip@gmail.com', 4),
('0151', 'Andre Silva', 'L', 'Sevilla', '2000-09-12', 'andresi@gmail.com', 2),
('0161', 'Olivia Dinata', 'P', 'Tokyo', '2001-10-11', 'olip@gmail.com', 3),
('0201', 'Anakku Hud', 'L', 'Jakarta', '2000-10-01', 'ahud@gmail.com', 5),
('0252', 'Sunarsih', 'P', 'Gombong', '2001-03-20', 'sunar213@gmail.com', 5);

-- --------------------------------------------------------

--
-- Table structure for table `pengajuan_cuti`
--

CREATE TABLE `pengajuan_cuti` (
  `id` int(11) NOT NULL,
  `nip` varchar(10) NOT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `lokasi_cuti` varchar(100) DEFAULT NULL,
  `alasan_cuti` varchar(45) DEFAULT NULL,
  `kategori_cuti_id` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pengajuan_cuti`
--

INSERT INTO `pengajuan_cuti` (`id`, `nip`, `tgl_mulai`, `tgl_selesai`, `jumlah`, `lokasi_cuti`, `alasan_cuti`, `kategori_cuti_id`, `status`) VALUES
(1, '0161', '2017-11-01', '2017-11-02', 2, 'depok', 'acara keluarga', 1, 1),
(2, '0151', '2017-11-06', '2017-11-10', 4, 'bali', 'libuaran keluarga', 1, 1),
(3, '0201', '2017-11-10', '2017-11-11', 2, 'jakarta', 'acara keluarga', 1, 1),
(4, '0201', '2017-12-27', '2017-12-30', 3, 'yogya', 'liburan keluarga', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) DEFAULT NULL,
  `jam_masuk` time DEFAULT NULL,
  `jam_keluar` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`id`, `nama`, `jam_masuk`, `jam_keluar`) VALUES
(1, 'Shift 1', '07:30:00', '16:30:00'),
(2, 'Shift 2', '08:00:00', '17:00:00'),
(3, 'Shift 3', '13:00:00', '21:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_absensi_pegawai1_idx` (`nip`),
  ADD KEY `fk_absensi_shift1_idx` (`shift_id`);

--
-- Indexes for table `divisi`
--
ALTER TABLE `divisi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history_pendidikan`
--
ALTER TABLE `history_pendidikan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_history_pendidikan_pegawai1_idx` (`nip`),
  ADD KEY `fk_history_pendidikan_jenjang_pendidikan1_idx` (`jenjang_id`);

--
-- Indexes for table `jenis_kegiatan`
--
ALTER TABLE `jenis_kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenjang_pendidikan`
--
ALTER TABLE `jenjang_pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_cuti`
--
ALTER TABLE `kategori_cuti`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_lembur`
--
ALTER TABLE `kategori_lembur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_kegiatan_pegawai1_idx` (`nip_pic`),
  ADD KEY `fk_kegiatan_jenis_kegiatan1_idx` (`jenis_id`);

--
-- Indexes for table `lembur`
--
ALTER TABLE `lembur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_lembur_pegawai1_idx` (`nip`),
  ADD KEY `fk_lembur_kategori_lembur1_idx` (`kategori_lembur_id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`nip`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_pegawai_divisi_idx` (`divisi_id`);

--
-- Indexes for table `pengajuan_cuti`
--
ALTER TABLE `pengajuan_cuti`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pengajuan_cuti_pegawai1_idx` (`nip`),
  ADD KEY `fk_pengajuan_cuti_kategori_cuti1_idx` (`kategori_cuti_id`);

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `divisi`
--
ALTER TABLE `divisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `history_pendidikan`
--
ALTER TABLE `history_pendidikan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jenis_kegiatan`
--
ALTER TABLE `jenis_kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jenjang_pendidikan`
--
ALTER TABLE `jenjang_pendidikan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kategori_cuti`
--
ALTER TABLE `kategori_cuti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kategori_lembur`
--
ALTER TABLE `kategori_lembur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lembur`
--
ALTER TABLE `lembur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pengajuan_cuti`
--
ALTER TABLE `pengajuan_cuti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `absensi`
--
ALTER TABLE `absensi`
  ADD CONSTRAINT `fk_absensi_pegawai1` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_absensi_shift1` FOREIGN KEY (`shift_id`) REFERENCES `shift` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history_pendidikan`
--
ALTER TABLE `history_pendidikan`
  ADD CONSTRAINT `fk_history_pendidikan_jenjang_pendidikan1` FOREIGN KEY (`jenjang_id`) REFERENCES `jenjang_pendidikan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_history_pendidikan_pegawai1` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD CONSTRAINT `fk_kegiatan_jenis_kegiatan1` FOREIGN KEY (`jenis_id`) REFERENCES `jenis_kegiatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kegiatan_pegawai1` FOREIGN KEY (`nip_pic`) REFERENCES `pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lembur`
--
ALTER TABLE `lembur`
  ADD CONSTRAINT `fk_lembur_kategori_lembur1` FOREIGN KEY (`kategori_lembur_id`) REFERENCES `kategori_lembur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_lembur_pegawai1` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD CONSTRAINT `fk_pegawai_divisi` FOREIGN KEY (`divisi_id`) REFERENCES `divisi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pengajuan_cuti`
--
ALTER TABLE `pengajuan_cuti`
  ADD CONSTRAINT `fk_pengajuan_cuti_kategori_cuti1` FOREIGN KEY (`kategori_cuti_id`) REFERENCES `kategori_cuti` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pengajuan_cuti_pegawai1` FOREIGN KEY (`nip`) REFERENCES `pegawai` (`nip`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
