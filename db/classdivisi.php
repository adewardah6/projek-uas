<?php
/*
+------+---------------+--------+-----------+------------+--------------------+-----------+
| nip  | nama          | gender | tmp_lahir | tgl_lahir  | email              | divisi_id |
+------+---------------+--------+-----------+------------+--------------------+-----------+
*/
    require_once "DAOdivisi.php";
    class divisi extends DAOdivisi
    {
        public function __construct()
        {
            parent::__construct("divisi");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id, nama, alamat) ".
            " VALUES (?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET id=?, nama=?, alamat=?" .
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik

    }
?>
