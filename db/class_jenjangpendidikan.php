<?php
/*
+------+---------------+--------+-----------+------------+--------------------+-----------+
| nip  | nama          | gender | tmp_lahir | tgl_lahir  | email              | divisi_id |
+------+---------------+--------+-----------+------------+--------------------+-----------+
*/
    require_once "DAO_jenjangpendidikan.php";
    class jenjang_pendidikan extends DAO_jenjangpendidikan
    {
        public function __construct()
        {
            parent::__construct("jenjang_pendidikan");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id, nama) ".
            " VALUES (?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET id=?, nama=?" .
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik

    }
?>
