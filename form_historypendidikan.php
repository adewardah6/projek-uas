<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_historypendidikan.php';
    //buat variabel untuk memanggil class
    $obj_kegiatan = new history_pendidikan();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_kegiatan->findByID($_idedit);
    }else{
        $data = [];
    }
?>

<script type="text/javascript" src="js/form_validasi_mahasiswa.js"></script>
<form class="form-horizontal" method="POST" name="formmahasiswa" action="proses_historypendidikan.php" >
<fieldset>
  <div id="book" class="page-section">
              <div class="container">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="section-heading">
  <!-- Form Name -->
  <legend align="center"><h1>Form History Pendidikan</h1></legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kode">ID</label>
  <div class="col-md-4">
  <input id="id" name="id" type="number" placeholder="Masukkan id" class="form-control input-md" value="<?php echo $data['id']?>">

  </div>
</div>

<!-- Text input

-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">Tahun Lulus</label>
  <div class="col-md-4">
  <input id="tahun_lulus" name="tahun_lulus" type="text" placeholder="Masukkan tahun lulus" class="form-control input-md" value="<?php echo $data['tahun_lulus']?>" >
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">Sekolah</label>
  <div class="col-md-4">
  <input id="sekolah" name="sekolah" type="text" placeholder="Masukkan sekolah" class="form-control input-md" value="<?php echo $data['sekolah']?>" >
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" for="judul">Jurusan</label>
  <div class="col-md-4">
  <input id="jurusan" name="jurusan" type="text" placeholder="Masukkan jurusan" class="form-control input-md" value="<?php echo $data['jurusan']?>" >
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">No Ijazah</label>
  <div class="col-md-4">
  <input id="noijazah" name="noijazah" type="text" placeholder="Masukkan No Ijazah" class="form-control input-md" value="<?php echo $data['noijazah']?>" >
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">Nip</label>
  <div class="col-md-4">
  <input id="nip" name="nip" type="text" placeholder="Masukkan nip" class="form-control input-md" value="<?php echo $data['nip']?>" >
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">Jenjang ID</label>
  <div class="col-md-4">
  <input id="jenjang_id" name="jenjang_id" type="number" placeholder="Masukkan jenjang id" class="form-control input-md" value="<?php echo $data['jenjang_id']?>" >
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" id="form-submit" class="btn" name="proses" class="btn" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
