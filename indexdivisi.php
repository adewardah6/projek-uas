<?php

/*+------+---------------+--------+-----------+------------+--------------------+-----------+
| nip  | nama          | gender | tmp_lahir | tgl_lahir  | email              | divisi_id |
+------+---------------+--------+-----------+------------+--------------------+-----------+
*/
    include_once 'top.php';
    require_once 'db/classdivisi.php';
    $obj = new divisi();
    $rows = $obj->getAll();
    ?>

    <!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
    <h2 align="center">Daftar Divisi</h2>
    <div class="line-dec"></div>
    <script type="text/javascript">
        $(document).ready(function(){
          $('#mahasiswa').DataTable();
        });
    </script>

    <table id="mahasiswa" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
        <thead>
        <tr class="danger">
            <th>ID</th><th>NAMA</th><th>ALAMAT</th><<th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $nomor = 1;
        foreach($rows as $row){


            echo '<td>'.$row['id'].'</td>';
            echo '<td class="success">'.$row['nama'].'</td>';
            echo '<td>'.$row['alamat'].'</td>';
            echo '<td><a href="viewdivisi.php?id='.$row['id']. '">View</a> |';
            echo '<a href="form_divisi.php?id='.$row['id']. '">Update</a></td>';
            echo '</tr>';
           $nomor++;
        }
        ?>
        </tbody>
    </table>
    <div class="panel-header" class="col-md-3">
        <a class="btn icon-btn btn-primary" href="form_divisi.php">
        <span class="glyphicon btn-glyphicon glyphicon-plus img-circle text-warning"></span>Tambah Divisi</a>

</div>
