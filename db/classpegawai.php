<?php
/*
+------+---------------+--------+-----------+------------+--------------------+-----------+
| nip  | nama          | gender | tmp_lahir | tgl_lahir  | email              | divisi_id |
+------+---------------+--------+-----------+------------+--------------------+-----------+
*/
    require_once "DAOpegawai.php";
    class pegawai extends DAOpegawai
    {
        public function __construct()
        {
            parent::__construct("pegawai");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (nip, nama, gender, tmp_lahir, tgl_lahir, email, divisi_id) ".
            " VALUES (?,?,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET nip=?, nama=?, gender=?, tmp_lahir=?, tgl_lahir=?, email=?, divisi_id=?" .
            " WHERE nip=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik
        public function getStatistik(){
        $sql = "select pegawai.nama, count(history_pendidikan.tahun_lulus) as jumlah from pegawai left join history_pendidikan on pegawai.id = history_pendidikan.id group by history_pendidikan.nama";
        $ps = $this->koneksi->prepare($sql);
        $ps->execute();
        return $ps->fetchAll();
      }
    }
?>
