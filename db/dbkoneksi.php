<?php
    class DBKoneksi{
        private $SERVER = "localhost";
        private $USERNAME = 'siswa';
        private $PASSWORD = '123456';
        private $DATABASE = 'dbhrd';
        private $koneksi;

            public function __construct(){
                try {
                $dsn = "mysql:host=" . $this->SERVER . ";dbname=" . $this->DATABASE;
                $this->koneksi = new PDO ($dsn, $this->USERNAME, $this->PASSWORD);
                } catch (PDOEXception $e) {
                  echo $e->getMessage();
                }
            }

            function getKoneksi() {
              return $this->koneksi;
            }
        }
        ?>
