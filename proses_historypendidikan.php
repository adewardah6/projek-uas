<?php
    require_once 'db/class_historypendidikan.php';
/*
+------+---------------+--------+-----------+------------+--------------------+-----------+
| nip  | nama          | gender | tmp_lahir | tgl_lahir  | email              | divisi_id |
+------+---------------+--------+-----------+------------+--------------------+-----------+
*/
    $obj = new history_pendidikan();

    $_id = $_POST['id'];
    $_tahun_lulus = $_POST['tahun_lulus'];
    $_sekolah = $_POST['sekolah'];
    $_jurusan = $_POST['jurusan'];
    $_noijazah = $_POST['noijazah'];
    $_nip = $_POST['nip'];
    $_jenjang_id = $_POST['jenjang_id'];
    $_proses = $_POST['proses'];

    $ar_data[] = $_id;
    $ar_data[] = $_tahun_lulus;
    $ar_data[] = $_sekolah;
    $ar_data[] = $_jurusan;
    $ar_data[] = $_noijazah;
    $ar_data[] = $_nip;
    $ar_data[] = $_jenjang_id;

    //buat operasi jika memilih button simpan, update atau hapus
    $row = 0;
    if($_proses == "Simpan"){
        $row = $obj->simpan($ar_data);
    }elseif($_proses == "Update"){
        $_idedit = $_POST['idedit'];
        $ar_data[] = $_idedit;
        $row = $obj->ubah($ar_data);
    }elseif($_proses == "Hapus"){
        unset($ar_data);
        $_idedit = $_POST['idedit'];
        $row = $obj->hapus($_idedit);
    }
    //handeler jika gagal atau sukses
    if($row==0){
        echo "Gagal Proses";
    }else{
        //echo "Proses Sukses";
        //langsung direct ke daftar_kegiatan.php
        header('Location:index_historypendidikan.php');
    }
