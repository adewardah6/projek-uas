<?php

/*+------+---------------+--------+-----------+------------+--------------------+-----------+
| nip  | nama          | gender | tmp_lahir | tgl_lahir  | email              | divisi_id |
+------+---------------+--------+-----------+------------+--------------------+-----------+
*/
    include_once 'top.php';
    require_once 'db/class_historypendidikan.php';
    $obj = new history_pendidikan();
    $rows = $obj->getAll();
    ?>

    <!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable-->
    <h2 align="center">Daftar History Pendidikan</h2>
    <div class="line-dec"></div>
    <script type="text/javascript">
        $(document).ready(function(){
          $('#mahasiswa').DataTable();
        });
    </script>

    <table id="mahasiswa" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
        <thead>
        <tr class="danger">
            <th>ID</th><th>Tahun Lulus</th><th>Sekolah</th><th>Jurusan</th><th>No Ijazah</th><th>NIP</th><th>Jenjang ID</th><th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $nomor = 1;
        foreach($rows as $row){
            echo '<tr><td>'.$row ['id'].'</td>';
            echo '<td class="success">'.$row['tahun_lulus'].'</td>';
            echo '<td>'.$row['sekolah'].'</td>';
            echo '<td class="success">'.$row['jurusan'].'</td>';
            echo '<td>'.$row['noijazah'].'</td>';
            echo '<td class="success">'.$row['nip'].'</td>';
            echo '<td>'.$row['jenjang_id'].'</td>';
            echo '<td><a href="view_historypendidikan.php?id='.$row['id']. '">View</a> |';
            echo '<a href="form_historypendidikan.php?id='.$row['id']. '">Update</a></td>';
            echo '</tr>';
           $nomor++;
        }
        ?>
        </tbody>
    </table>
    <div class="panel-header" class="col-md-3">
        <a class="btn icon-btn btn-primary" href="form_historypendidikan.php">
        <span class="glyphicon btn-glyphicon glyphicon-plus img-circle text-warning"></span>Tambah History Pendidikan </a>

</div>
