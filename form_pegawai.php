<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/classpegawai.php';
    //buat variabel untuk memanggil class
    $obj_kegiatan = new pegawai();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_kegiatan->findByID($_idedit);
    }else{
        $data = [];
    }
?>

<script type="text/javascript" src="js/form_validasi_mahasiswa.js"></script>
<form class="form-horizontal" method="POST" name="formmahasiswa" action="prosespegawai.php" >
<fieldset>
  <div id="book" class="page-section">
              <div class="container">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="section-heading">
  <!-- Form Name -->
  <legend align="center"><h1>Form Pegawai</h1></legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kode">NIP</label>
  <div class="col-md-4">
  <input id="nip" name="nip" type="number" placeholder="Masukkan nip" class="form-control input-md" value="<?php echo $data['nip']?>">

  </div>
</div>

<!-- Text input

-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">Nama</label>
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="Masukkan nama" class="form-control input-md" value="<?php echo $data['nama']?>" >
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">gender</label>
  <div class="col-md-4">
  <input id="gender" name="gender" type="text" placeholder="Masukkan tanggal lahir" class="form-control input-md" value="<?php echo $data['gender']?>" >
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" for="judul">tmp lahir</label>
  <div class="col-md-4">
  <input id="tmp_lahir" name="tmp_lahir" type="text" placeholder="Masukkan jenis kelamin" class="form-control input-md" value="<?php echo $data['tmp_lahir']?>" >
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">tgl lahir</label>
  <div class="col-md-4">
  <input id="tgl_lahir" name="tgl_lahir" type="date" placeholder="Masukkan prodi" class="form-control input-md" value="<?php echo $data['tgl_lahir']?>" >
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">email</label>
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="Masukkan tahun masuk" class="form-control input-md" value="<?php echo $data['email']?>" >
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">devisi ID</label>
  <div class="col-md-4">
  <input id="divisi_id" name="divisi_id" type="number" placeholder="Masukkan rombel id" class="form-control input-md" value="<?php echo $data['divisi_id']?>" >
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" id="form-submit" class="btn" name="proses" class="btn" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
