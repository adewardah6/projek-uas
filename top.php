<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Modul HRD</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

	<link href="css/bootstrap_united.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">

    <link href="css/style.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">

					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> <a class="navbar-brand" href="#">Kepegawaian HRD</a>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active">
							<a href="indexpegawai.php">Pegawai</a>
						</li>
						<li>
							<a href="indexdivisi.php">Divisi</a>
						</li>
            <li class="divider">
            <li>
              <a href="index_jenjangpendidikan.php">Jenjang Pendidikan</a>
            </li>
            <li class="divider">
            <li>
              <a href="index_historypendidikan.php">Riwayat Pendidikan</a>
            </li>
            <li>
              <a href="grafikpegawai.php">Grafik</a>
            </li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Master Data<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="tabelpegawai.php">Pegawai</a>
								</li>
                <li class="divider">
								<li>
									<a href="tabeldivisi.php">Divisi</a>
								</li>
                <li class="divider">
								<li>
									<a href="jenjang_pendidikan.php">Jenjang Pendidikan</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="history_pendidikan.php">Histori Pendidikan</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">One more separated link</a>
								</li>
							</ul>
						</li>
					</ul>
					<form class="navbar-form navbar-left" role="search">
						<div class="form-group">
							<input class="form-control" type="text">
						</div>
						<button type="submit" class="btn btn-default">
							Submit
						</button>
					</form>
				</div>

			</nav>
		</div>
	</div>
